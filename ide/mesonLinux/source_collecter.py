import os

fileTypes = [".cpp", ".c"]

def run(path):
	for root, dirs, files in os.walk(path):
		for file in files:
			if( any( map( lambda ext : file.endswith(ext), fileTypes ) ) ):
				print ( os.path.join(root, file) )

#relative to this script
run(os.path.join('..','..','src'))
run(os.path.join('..','..','include'))