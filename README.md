
# Bitset

A simple bitset with optimisations for operations which involve iterating over the full set, for example: bsf, bsr and count.

## Getting started

This repository is intended to be used as a submodule, add the folder:
```
/include/
```
to your project and then:
```
#include <bitset.hpp>
```

## Dependencies
This project depends on:

 - C++14 compiler
 - Catch2
 - hayai

## Running the tests / benchmark
Once the sln has been built you can run the tests by executing:
```
ide/msvs2015/x64/Release/bitset.exe
```
Once the sln has been build you can run the benchmarks by executing:
```
ide/msvs2015/x64/Release/ bitset-bench.exe
```

## Performance
As you can see, bitset_tiered (the optimised bitset) is ~54 times faster than bitset_flat (a usual implementation of bitset):
![alt tag](https://bytebucket.org/QuantumKarl/bitset/raw/a423f8a2c889fd2184d705025d0556d1a83f688e/doc/flat_vs_tiered.png?token=db328ec852f222da0c0b5f12cd4cac385bd7e849)

## License

GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
