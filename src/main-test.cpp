/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#if defined( CATCH_CONFIG_MAIN )
#endif
#include <iostream>
#include <random>
#include <algorithm>
#include <set>

#include "../include/bitset.hpp"
#include "tests.hpp"

//--------------------------------
TEST_CASE( "flat_test0", "[bitset][basic]" )
{
    test0< bitset_flat< 128 > >( );
    test1< bitset_flat< 2 > >( );
    test1< bitset_flat< 640 > >( );
    test1< bitset_flat< 66 > >( );
}

//--------------------------------
TEST_CASE( "tiered_test0", "[bitset][basic]" )
{
    test0< bitset_tiered< 128 > >( );
    test1< bitset_tiered< 2 > >( );
    test1< bitset_tiered< 8192 > >( );
    test1< bitset_tiered< 4097 > >( );
}

//--------------------------------
TEST_CASE( "flat_test1", "[bitset]" )
{
    test1< bitset_flat< 128 > >( );
    test1< bitset_flat< 2 > >( );
    test1< bitset_flat< 640 > >( );
    test1< bitset_flat< 66 > >( );
}

//--------------------------------
TEST_CASE( "tiered_test1", "[bitset]" )
{
    test1< bitset_tiered< 128 > >( );
    test1< bitset_tiered< 2 > >( );
    test1< bitset_tiered< 8192 > >( );
    test1< bitset_tiered< 4097 > >( );
}

//--------------------------------
TEST_CASE( "flat_test2", "[bitset]" )
{
    test2< bitset_flat< 128 > >( );
    test2< bitset_flat< 2 > >( );
    test2< bitset_flat< 640 > >( );
    test2< bitset_flat< 66 > >( );
}

//--------------------------------
TEST_CASE( "tiered_test2", "[bitset]" )
{
    test2< bitset_tiered< 128 > >( );
    test2< bitset_tiered< 2 > >( );
    test2< bitset_tiered< 8192 > >( );
    test2< bitset_tiered< 4098 > >( );
}

//--------------------------------
TEST_CASE( "flat_test3", "[bitset]" )
{
    test3< bitset_flat< 128 > >( 0.5 );
    test3< bitset_flat< 20 > >( 0.6 );
    test3< bitset_flat< 640 > >( 0.5 );
    test3< bitset_flat< 777 > >( 0.75 );
}

//--------------------------------
TEST_CASE( "tiered_test3", "[bitset]" )
{
    test3< bitset_tiered< 128 > >( 0.5 );
    test3< bitset_tiered< 200 > >( 0.6 );
    test3< bitset_tiered< 8192 > >( 0.5 );
    test3< bitset_tiered< 7777 > >( 0.75 );
}

//--------------------------------
TEST_CASE( "flat_test4", "[bitset]" )
{
    test4< bitset_flat< 128 > >( 56 );
    test4< bitset_flat< 20 > >( 5 );
    test4< bitset_flat< 640 > >( 250 );
    test4< bitset_flat< 777 > >( 344 );
}

//--------------------------------
TEST_CASE( "tiered_test4", "[bitset]" )
{
    test4< bitset_tiered< 128 > >( 56 );
    test4< bitset_tiered< 200 > >( 50 );
    test4< bitset_tiered< 8192 > >( 4600 );
    test4< bitset_tiered< 7777 > >( 3544 );
}

//--------------------------------
TEST_CASE( "flat_mixed", "[bitset][large]" )
{
    constexpr size_t const bits = 0x01 << 17;
    test1< bitset_flat< bits > >( );
    test2< bitset_flat< bits > >( );
    test3< bitset_flat< bits > >( 0.75 );
    test4< bitset_flat< bits > >(
        static_cast< size_t >( static_cast< double >( bits ) * 0.75 ) );
}

//--------------------------------
TEST_CASE( "tiered_mixed", "[bitset][large]" )
{
    constexpr size_t const bits = 0x01 << 17;
    test1< bitset_tiered< bits > >( );
    test2< bitset_tiered< bits > >( );
    test3< bitset_tiered< bits > >( 0.75 );
    test4< bitset_tiered< bits > >(
        static_cast< size_t >( static_cast< double >( bits ) * 0.75 ) );
}