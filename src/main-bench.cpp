/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
#include <cassert>
#include <fstream>
#include <hayai.hpp>
#include <random>

#include "../include/bitset.hpp"
#include "benches.hpp"

//----------------------------------------------------------------

// the same across all cases to make them comparable
namespace cases
{
constexpr size_t const runs       = 4;
constexpr size_t const iterations = 2;

constexpr size_t const bit_count = 0x1 << 22;
}

// 100% density, every bit is set
namespace case1
{
static size_t constexpr options_mask = toUType( setup_options::one_all );
} // end of namespace case1

// NOTE: the #defines for BENCHMARK_F do not compile if the fixture is inside a namespace.
using flat1   = fixture< bitset_flat< cases::bit_count >, case1::options_mask >;
using tiered1 = fixture< bitset_tiered< cases::bit_count >, case1::options_mask >;

//--------------------------------
BENCHMARK_F( flat1, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( tiered1, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( flat1, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
BENCHMARK_F( tiered1, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
/* 50% density, approx 50% of the bits are set
*/
namespace case2
{
static size_t constexpr options_mask =
    toUType( setup_options::zero_all ) | toUType( setup_options::set_random_half );
} // end of namespace case2

using flat2   = fixture< bitset_flat< cases::bit_count >, case2::options_mask >;
using tiered2 = fixture< bitset_tiered< cases::bit_count >, case2::options_mask >;

//--------------------------------
BENCHMARK_F( flat2, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( tiered2, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( flat2, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
BENCHMARK_F( tiered2, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
/* 50% density, approx 50% of the bits are not set
*/
namespace case3
{
static size_t constexpr options_mask =
    toUType( setup_options::one_all ) | toUType( setup_options::set_random_half );
} // end of namespace case3

using flat3   = fixture< bitset_flat< cases::bit_count >, case3::options_mask >;
using tiered3 = fixture< bitset_tiered< cases::bit_count >, case3::options_mask >;

//--------------------------------
BENCHMARK_F( flat3, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( tiered3, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( flat3, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
BENCHMARK_F( tiered3, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
/* 50% density, approx 50% of the bits are set, then flipped
 if the random number generator does not generate random numbers next to each-other,
 this test should have intervals of set bits
 */
namespace case4
{
static size_t constexpr options_mask = toUType( setup_options::zero_all ) |
                                       toUType( setup_options::set_random_half ) |
                                       toUType( setup_options::flip_all );
} // end of namespace case4

using flat4   = fixture< bitset_flat< cases::bit_count >, case4::options_mask >;
using tiered4 = fixture< bitset_tiered< cases::bit_count >, case4::options_mask >;

//--------------------------------
BENCHMARK_F( flat4, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( tiered4, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( flat4, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
BENCHMARK_F( tiered4, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
/* 50% density, approx 50% of the bits are not set, then flipped
 if the random number generator does not generate random numbers next to each-other,
 this test should have intervals of set not bits
 */
namespace case5
{
static size_t constexpr options_mask = toUType( setup_options::one_all ) |
                                       toUType( setup_options::set_random_half ) |
                                       toUType( setup_options::flip_all );
} // end of namespace case4

using flat5   = fixture< bitset_flat< cases::bit_count >, case5::options_mask >;
using tiered5 = fixture< bitset_tiered< cases::bit_count >, case5::options_mask >;

//--------------------------------
BENCHMARK_F( flat5, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( tiered5, bsf, cases::runs, cases::iterations )
{
    bsf( );
}

//--------------------------------
BENCHMARK_F( flat5, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//--------------------------------
BENCHMARK_F( tiered5, bsr, cases::runs, cases::iterations )
{
    bsr( );
}

//----------------------------------------------------------------
int
    main( )
{
    // start cases
    std::fstream         jsonFile( "results.json", std::fstream::out );
    hayai::JsonOutputter jsonOutputter( jsonFile );
    hayai::Benchmarker::AddOutputter( jsonOutputter );

    hayai::ConsoleOutputter consoleOutputter;
    hayai::Benchmarker::AddOutputter( consoleOutputter );

    hayai::Benchmarker::RunAllTests( );

    return EXIT_SUCCESS;
}