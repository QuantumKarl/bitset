/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_TESTS_HPP )
#define D_TESTS_HPP

#if 0
class e
{
  public:
};

//--------------------------------
// compilation test, it is unused
int
    f( )
{
    bitset< e >        bsete; // calls default implementation (aka empty / assert false)
    bitset_flat< 100 > bsetf; // calls flat implementation (aka single array of words)
    bitset_tiered< 100 >
        bsett; // calls tiered implementation (aka two arrays of words, where the second are set flags for each word of the previous)

    bsetf.all( );
    bsete.all( );
    bsete.any( );
    bsete.bsf( );
    bsete.bsr( );
    bsete.count( );
    bsete.none( );
    bsete.read_at( 0 );
    bsete.write_at( 0, true );
    bsete.one_all( );
    bsete.zero_all( );
    bsete.flip_all( );
    bsete.npos( );
    bsete.size( );

    return 0;
}
#endif

template < typename bitset_t >
void
    test0( )
{
    bitset_t               b;
    constexpr size_t const bits = bitset_t::size( );

    //----------------
    // lowest index
    b.write_at( 0, true );
    REQUIRE( b.read_at( 0 ) == true );

    b.write_at( 0, false );
    REQUIRE( b.read_at( 0 ) == false );

    //----------------
    // first boundary
    b.write_at( 63, true );
    REQUIRE( b.read_at( 63 ) == true );

    b.write_at( 63, false );
    REQUIRE( b.read_at( 63 ) == false );

    //----------------
    // second boundary
    b.write_at( 127, true );
    REQUIRE( b.read_at( 127 ) == true );

    b.write_at( 127, false );
    REQUIRE( b.read_at( 127 ) == false );

    //--------------------------------
    // all locations
    b.zero_all( );
    for ( size_t i = 1; i < bits; ++i )
    {
        b.write_at( i, !b.read_at( i - 1 ) );
        REQUIRE( b.read_at( i ) == !b.read_at( i - 1 ) );
    }
}

//--------------------------------
/* test1
	tests the following functions:
	one_all, zero_all, flip_all,
	count, bsf, bsr
*/
template < typename bitset_t >
void
    test1( )
{
    bitset_t               b;
    constexpr size_t const bits = bitset_t::size( );
    constexpr size_t const mid  = bits / 2;

    b.one_all( );
    REQUIRE( b.count( ) == bits );
    REQUIRE( b.bsf( ) == 0 );
    REQUIRE( b.bsr( ) == bits - 1 );

    b.write_at( mid, false );
    b.flip_all( );
    REQUIRE( b.count( ) == 1 );
    REQUIRE( b.bsf( ) == mid );
    REQUIRE( b.bsr( ) == mid );

    b.zero_all( );
    b.write_at( mid, true );

    REQUIRE( b.count( ) == 1 );
    REQUIRE( b.bsf( ) == mid );
    REQUIRE( b.bsr( ) == mid );
}

//--------------------------------
/* test2
	tests the following functions:
	write_at, read_at, count, flip_all,
	all, any, none
*/
template < typename bitset_t >
void
    test2( )
{
    bitset_t               b;
    constexpr size_t const bits = bitset_t::size( );
    constexpr size_t const half_size =
        static_cast< size_t >( static_cast< double >( bits ) / 2.0 );
    static_assert( bits % 2 == 0, "number of bits requires to be divisible by 2" );

    for ( size_t i = 1; i < bits; ++i )
    {
        b.write_at( i, !b.read_at( i - 1 ) );
        REQUIRE( b.read_at( i ) == !b.read_at( i - 1 ) );
    }
    REQUIRE( b.count( ) == half_size );
    b.flip_all( );
    REQUIRE( b.count( ) == half_size );
    REQUIRE( b.all( ) == false );
    REQUIRE( b.any( ) == true );
    REQUIRE( b.none( ) == false );
}

//--------------------------------
/* test3
	tests the following functions:
	write_at, read_at, bsf, bsr, count, all, any, none
*/
template < typename bitset_t >
void
    test3( double const fill_ratio )
{
    bitset_t               b;
    constexpr size_t const bits = bitset_t::size( );
    size_t const           count =
        static_cast< size_t >( static_cast< double >( bits ) * fill_ratio + 0.5 );

    std::mt19937_64                         gen1;
    std::uniform_int_distribution< size_t > dist_index( 0, bits - 1 );

    size_t             min = std::numeric_limits< size_t >::max( );
    size_t             max = std::numeric_limits< size_t >::min( );
    std::set< size_t > indices;
    for ( size_t i = 0; i < count; ++i )
    {
        size_t const index = dist_index( gen1 );
        min                = index < min ? index : min;
        max                = index > max ? index : max;
        b.write_at( index, std::true_type( ) );
        REQUIRE( b.read_at( index ) == true );
        REQUIRE( b.bsf( ) == min );
        REQUIRE( b.bsr( ) == max );
        indices.insert( index );
        REQUIRE( b.count( ) == indices.size( ) );
    }

    for ( auto index : indices )
    {
        b.write_at( index, std::false_type( ) );
        REQUIRE( b.read_at( index ) == false );
    }

    REQUIRE( b.all( ) == false );
    REQUIRE( b.any( ) == false );
    REQUIRE( b.none( ) == true );
}

//--------------------------------
/* test4
	tests the following functions:
	one_all, zero_all, all, any, none, write_at, bsf
*/
template < typename bitset_t >
void
    test4( size_t const insert_count )
{
    bitset_t               b;
    constexpr size_t const bits = bitset_t::size( );
    assert( insert_count < bits );

    std::mt19937_64                         gen1;
    std::uniform_int_distribution< size_t > dist_index( 0, bits - 1 );

    b.one_all( );
    REQUIRE( b.all( ) == true );
    REQUIRE( b.any( ) == true );
    REQUIRE( b.none( ) == false );

    for ( size_t i = 0; i < insert_count; ++i )
    {
        size_t const index = dist_index( gen1 );
        b.write_at( index, false );
    }
    REQUIRE( b.all( ) == false );
    REQUIRE( b.any( ) == true );
    REQUIRE( b.none( ) == false );

    size_t index = 0;
    while ( ( index = b.bsf( ) ) != bitset_t::npos( ) )
    {
        b.write_at( index, false );
    }

    REQUIRE( b.all( ) == false );
    REQUIRE( b.any( ) == false );
    REQUIRE( b.none( ) == true );
}

#endif