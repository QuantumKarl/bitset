/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_BENCHES_HPP )
#define D_BENCHES_HPP

//--------------------------------
/* toUType( enumerator )
	Given an enumeration of a scoped enum, get its value.

	NOTE: borrowed from page 73 from More effective C++ by Scott Meyers (9th print)
	NOTE: C++14 version
*/
template < typename E >
constexpr auto
    toUType( E enumerator ) noexcept
{
    return static_cast< std::underlying_type_t< E > >( enumerator );
}

//--------------------------------
enum class setup_options : size_t
{
    zero_all                 = 0x1 << 0,
    one_all                  = 0x1 << 1,
    set_random_random        = 0x1 << 2,
    set_random_quarter       = 0x1 << 3,
    set_random_half          = 0x1 << 4,
    set_random_three_quarter = 0x1 << 5,
    flip_all                 = 0x1 << 6
};

//--------------------------------
template < typename bitset_t, size_t options = toUType( setup_options::zero_all ) >
class fixture : public ::hayai::Fixture
{
  public:
    //--------------------------------
    fixture( )
        : bitset_( )
    {
    }

    //--------------------------------
    void
        SetUp( ) override
    {
        constexpr size_t const opt      = options;
        bool                   opposite = true;
        if ( opt & toUType( setup_options::one_all ) )
        {
            bitset_.one_all( );
            opposite = false;
        }
        else if ( opt & toUType( setup_options::zero_all ) )
        {
            bitset_.zero_all( );
            opposite = true;
        }

        constexpr bool random = ( opt & toUType( setup_options::set_random_random ) ) > 0;
        constexpr bool quater = ( opt & toUType( setup_options::set_random_quarter ) ) > 0;
        constexpr bool half   = ( opt & toUType( setup_options::set_random_half ) ) > 0;
        constexpr bool three_quarter = ( opt & toUType( setup_options::set_random_half ) ) > 0;

        if ( random || quater || half || three_quarter )
        {
            std::mt19937_64 gen( 2 );

            double ratio = 0.5;
            if ( random )
            {
                std::uniform_real_distribution< double > dist(0.0,1.0);
                ratio = dist( gen );
                std::cout << "random ratio: " << ratio << std::endl;
            }
            else if ( quater )
            {
                ratio = 0.25;
            }
            else if ( half )
            {
                ratio = 0.5;
            }
            else if ( three_quarter )
            {
                ratio = 0.75;
            }

            constexpr size_t const min = 0u;
            constexpr size_t const max = bitset_t::size( ) - 1;

            std::uniform_int_distribution< uint64_t > dist( min, max );

            for ( size_t i = static_cast< size_t >( max * ratio + 0.5 ); i != 0; --i )
            {
                size_t const index = dist( gen );
                bitset_.write_at( index, opposite );
            }
        }

        if ( opt & toUType( setup_options::flip_all ) )
        {
            bitset_.flip_all( );
        }
    }

    //--------------------------------
    void
        TearDown( ) override
    {
    }

    //--------------------------------
    void
        bsf( )
    {
        auto&  bitset = bitset_;
        size_t index  = bitset_t::npos( );
        while ( ( index = bitset.bsf( ) ) != bitset_t::npos( ) )
        {
            bitset.write_at( index, std::false_type( ) );
        }
    }

    //--------------------------------
    void
        bsr( )
    {
        auto&  bitset = bitset_;
        size_t index  = bitset_t::npos( );
        while ( ( index = bitset.bsr( ) ) != bitset_t::npos( ) )
        {
            bitset.write_at( index, std::false_type( ) );
        }
    }

  private:
    bitset_t bitset_;
};

#endif