cd ..\\..\\src
find -name '*.cpp' -o -name '*.hpp' -o -name '*.inl' | xargs clang-format -i -style=file

cd ..\\include
find -name '*.cpp' -o -name '*.hpp' -o -name '*.inl' | xargs clang-format -i -style=file