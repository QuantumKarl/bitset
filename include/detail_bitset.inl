/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
/*	This file is part of Bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	Bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Bitset. If not, see <http://www.gnu.org/licenses/>.
*/
namespace detail_bitset
{
//----------------------------------------------------------------
// declare which functions are in the interface
// NOTE: _i for interface to reduce chance of conflicts with other functions
D_HAS_MEMBER_DETECT( all_i, bool, std::declval< T const& >( ).all( ) )
D_HAS_MEMBER_DETECT( any_i, bool, std::declval< T const& >( ).any( ) )
D_HAS_MEMBER_DETECT( none_i, bool, std::declval< T const& >( ).none( ) )
D_HAS_MEMBER_DETECT( count_i, size_t, std::declval< T const& >( ).bsf( ) )

D_HAS_MEMBER_DETECT( bsf_i, size_t, std::declval< T const& >( ).bsf( ) )
D_HAS_MEMBER_DETECT( bsr_i, size_t, std::declval< T const& >( ).bsr( ) )

D_HAS_MEMBER_DETECT( read_at_i,
                     bool,
                     std::declval< T const& >( ).read_at( std::declval< size_t const >( ) ) )
D_HAS_MEMBER_DETECT( write_at_i1,
                     void,
                     std::declval< T& >( ).write_at( std::declval< size_t const >( ),
                                                std::declval< bool const >( ) ) )
D_HAS_MEMBER_DETECT( write_at_i2,
                     void,
                     std::declval< T& >( ).write_at( std::declval< size_t const >( ),
                                                std::declval< std::true_type >( ) ) )
D_HAS_MEMBER_DETECT( write_at_i3,
                     void,
                     std::declval< T& >( ).write_at( std::declval< size_t const >( ),
                                                std::declval< std::false_type >( ) ) )

D_HAS_MEMBER_DETECT( set_all_i, void, std::declval< T& >( ).one_all( ) )
D_HAS_MEMBER_DETECT( unset_all_i, void, std::declval< T& >( ).zero_all( ) )

D_HAS_MEMBER_DETECT( flip_all_i, void, std::declval< T& >( ).flip_all( ) )

D_HAS_MEMBER_DETECT( npos_i, size_t, std::declval< T& >( ).npos( ) )
D_HAS_MEMBER_DETECT( size_i, size_t, std::declval< T& >( ).size( ) )

//--------------------------------
// shared functions
// TODO: move to sys_bitops namespace / file
// which also selects intrinsics correctly

//--------------------------------
constexpr int64_t
    ceil( double num )
{
    return ( static_cast< double >( static_cast< int64_t >( num ) ) == num )
               ? static_cast< int64_t >( num )
               : static_cast< int64_t >( num ) + ( num > 0 ? 1 : 0 );
}

//--------------------------------
/* WP3 Nifty
	NOTE: from Hacker's Delight by Henry S. Warren, Jr
*/
template < typename T >
static auto
    bitcount( T n )
{
    constexpr T const TEST_BITS = sizeof( T ) * 8;

    constexpr T const m1  = ( ~( T )0 ) / 3;   // binary: 0101...
    constexpr T const m2  = ( ~( T )0 ) / 5;   // binary: 00110011..
    constexpr T const m4  = ( ~( T )0 ) / 17;  // binary: 4 zeros,  4 ones ...
    constexpr T const h01 = ( ~( T )0 ) / 255; // the sum of 256 to the power of 0,1,2,3...

    n -= ( n >> 1 ) & m1;                 //put count of each 2 bits into those 2 bits
    n = ( n & m2 ) + ( ( n >> 2 ) & m2 ); //put count of each 4 bits into those 4 bits
    n = ( n + ( n >> 4 ) ) & m4;          //put count of each 8 bits into those 8 bits

    // return the left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ...
    return ( n * h01 ) >> ( TEST_BITS - 8 );
}

const unsigned char forward_index64[64] = { 0,  1,  48, 2,  57, 49, 28, 3,  61, 58, 50, 42, 38,
                                            29, 17, 4,  62, 55, 59, 36, 53, 51, 43, 22, 45, 39,
                                            33, 30, 24, 18, 12, 5,  63, 47, 56, 27, 60, 41, 37,
                                            16, 54, 35, 52, 21, 44, 32, 23, 11, 46, 26, 40, 15,
                                            34, 20, 31, 10, 25, 14, 19, 9,  13, 8,  7,  6 };

//--------------------------------
/*
	* bitScanForward
	* @author Martin L�uter (1997)
	*         Charles E. Leiserson
	*         Harald Prokop
	*         Keith H. Randall
	* "Using de Bruijn Sequences to Index a 1 in a Computer Word"
	* @param bb bitboard to scan
	* @precondition bb != 0
	* @return index (0..63) of least significant one bit
	*/
inline size_t
    bitScanForward( uint64_t const bb )
{
    constexpr uint64_t const debruijn64 = 0x03f79d71b4cb0a89ULL;
    assert( bb != 0 );
    return static_cast< size_t >( forward_index64[( ( bb & 0 - bb ) * debruijn64 ) >> 58] );
}

//--------------------------------
const unsigned char reverse_index64[64] = { 0,  47, 1,  56, 48, 27, 2,  60, 57, 49, 41, 37, 28,
                                            16, 3,  61, 54, 58, 35, 52, 50, 42, 21, 44, 38, 32,
                                            29, 23, 17, 11, 4,  62, 46, 55, 26, 59, 40, 36, 15,
                                            53, 34, 51, 20, 43, 31, 22, 10, 45, 25, 39, 14, 33,
                                            19, 30, 9,  24, 13, 18, 8,  12, 7,  6,  5,  63 };

//--------------------------------
/*
	* bitScanReverse
	* @authors Kim Walisch, Mark Dickinson
	* @param bb bitboard to scan
	* @precondition bb != 0
	* @return index (0..63) of most significant one bit
	*/
inline size_t
    bitScanReverse( uint64_t bb )
{
    constexpr uint64_t const debruijn64 = 0x03f79d71b4cb0a89ULL;
    assert( bb != 0 );
    bb |= bb >> 1;
    bb |= bb >> 2;
    bb |= bb >> 4;
    bb |= bb >> 8;
    bb |= bb >> 16;
    bb |= bb >> 32; //-V112
    return static_cast< size_t >( reverse_index64[( bb * debruijn64 ) >> 58] );
}

#include "bitset_flat.inl"
#include "bitset_tiered.inl"
}