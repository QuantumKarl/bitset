/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
/*	This file is part of Bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	Bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Bitset. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_BITSET_HPP )
#define D_BITSET_HPP
#include <array>
#include <cassert>
#include <algorithm>

#include <sys_SFINAE.hpp>

#include "detail_bitset.inl"

//----------------------------------------------------------------
template < typename impl_t >
class bitset
{
  public:
    bool   all( ) const;
    bool   any( ) const;
    bool   none( ) const;
    size_t count( ) const;

    size_t bsf( ) const;
    size_t bsr( ) const;

    bool read_at( size_t const i_index ) const;
    void write_at( size_t const i_index, bool const i_value ); // dynamic write

    void write_at( size_t const i_index, std::true_type );  // static write true
    void write_at( size_t const i_index, std::false_type ); // static write false

    void one_all( );
    void zero_all( );

    void flip_all( );

    static constexpr size_t npos( );
    static constexpr size_t size( );

  private:
    //--------------------------------
    bool all( std::true_type ) const;
    bool all( std::false_type ) const;

    //--------------------------------
    bool any( std::true_type ) const;
    bool any( std::false_type ) const;

    //--------------------------------
    bool none( std::true_type ) const;
    bool none( std::false_type ) const;

    //--------------------------------
    size_t count( std::true_type ) const;
    size_t count( std::false_type ) const;

    //--------------------------------
    size_t bsf( std::true_type ) const;
    size_t bsf( std::false_type ) const;

    //--------------------------------
    size_t bsr( std::true_type ) const;
    size_t bsr( std::false_type ) const;

    //--------------------------------
    bool read_at( size_t const i_index, std::true_type ) const;
    bool read_at( size_t const i_index, std::false_type ) const;

    //--------------------------------
    // dynamic write
    void write_at( size_t const i_index, bool const i_value, std::true_type );
    void write_at( size_t const i_index, bool const i_value, std::false_type );

    //--------------------------------
    // static write true
    void write_at( size_t const i_index, std::true_type, std::true_type );
    void write_at( size_t const i_index, std::true_type, std::false_type );

    //--------------------------------
    // static write false
    void
         write_at( size_t const i_index, std::false_type, std::true_type ); // static write false
    void write_at( size_t const i_index,
                   std::false_type,
                   std::false_type ); // static write false

    //--------------------------------
    void one_all( std::true_type );
    void one_all( std::false_type );

    //--------------------------------
    void zero_all( std::true_type );
    void zero_all( std::false_type );

    //--------------------------------
    void flip_all( std::true_type );
    void flip_all( std::false_type );

    //--------------------------------
    static constexpr size_t npos( std::true_type );
    static constexpr size_t npos( std::false_type );

    //--------------------------------
    static constexpr size_t size( std::true_type );
    static constexpr size_t size( std::false_type );

    impl_t impl_;
};

#include "bitset.inl"

template < size_t bit_count >
using bitset_flat = bitset< detail_bitset::flat< bit_count > >;
// NOTE: alt name could be bitset_small

template < size_t bit_count >
using bitset_tiered = bitset< detail_bitset::tiered< bit_count > >;
// NOTE: alt name could be bitset_large

#endif