/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
/*	This file is part of Bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	Bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Bitset. If not, see <http://www.gnu.org/licenses/>.
*/
//-------------------------------- public functions --------------------------------

//--------------------------------
template < typename impl_t >
bool
    bitset< impl_t >::all( ) const
{
    return all( detail_bitset::has_all_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
bool
    bitset< impl_t >::any( ) const
{
    return any( detail_bitset::has_any_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
bool
    bitset< impl_t >::none( ) const
{
    return none( detail_bitset::has_none_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
size_t
    bitset< impl_t >::count( ) const
{
    return count( detail_bitset::has_none_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
size_t
    bitset< impl_t >::bsf( ) const
{
    return bsf( detail_bitset::has_bsf_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
size_t
    bitset< impl_t >::bsr( ) const
{
    return bsr( detail_bitset::has_bsr_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
bool
    bitset< impl_t >::read_at( size_t const i_index ) const
{
    return read_at( i_index, detail_bitset::has_read_at_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const i_index, bool const i_value )
{
    return write_at( i_index, i_value, detail_bitset::has_write_at_i1< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const i_index, std::true_type )
{
    return write_at( i_index, std::true_type( ), detail_bitset::has_write_at_i2< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const i_index, std::false_type )
{
    return write_at( i_index, std::false_type( ), detail_bitset::has_write_at_i3< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::one_all( )
{
    one_all( detail_bitset::has_set_all_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::zero_all( )
{
    zero_all( detail_bitset::has_unset_all_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::flip_all( )
{
    flip_all( detail_bitset::has_flip_all_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
constexpr size_t
    bitset< impl_t >::npos( )
{
    return bitset< impl_t >::npos( detail_bitset::has_npos_i< impl_t >( ) );
}

//--------------------------------
template < typename impl_t >
constexpr size_t
    bitset< impl_t >::size( )
{
    return bitset< impl_t >::size( detail_bitset::has_size_i< impl_t >( ) );
}

//-------------------------------- private functions --------------------------------

//--------------------------------
template < typename impl_t >
bool bitset< impl_t >::all( std::true_type ) const
{
    return impl_.all( );
}

//--------------------------------
template < typename impl_t >
bool bitset< impl_t >::all( std::false_type ) const
{
    assert( false );
    return false;
}

//--------------------------------
template < typename impl_t >
bool bitset< impl_t >::any( std::true_type ) const
{
    return impl_.any( );
}

//--------------------------------
template < typename impl_t >
bool bitset< impl_t >::any( std::false_type ) const
{
    assert( false );
    return false;
}

//--------------------------------
template < typename impl_t >
bool bitset< impl_t >::none( std::true_type ) const
{
    return impl_.none( );
}

//--------------------------------
template < typename impl_t >
bool bitset< impl_t >::none( std::false_type ) const
{
    assert( false );
    return false;
}

//--------------------------------
template < typename impl_t >
size_t bitset< impl_t >::count( std::true_type ) const
{
    return impl_.count( );
}

//--------------------------------
template < typename impl_t >
size_t bitset< impl_t >::count( std::false_type ) const
{
    assert( false );
    return std::numeric_limits< size_t >::max( );
}

//--------------------------------
template < typename impl_t >
size_t bitset< impl_t >::bsf( std::true_type ) const
{
    return impl_.bsf( );
}

//--------------------------------
template < typename impl_t >
size_t bitset< impl_t >::bsf( std::false_type ) const
{
    assert( false );
    return npos( );
}

//--------------------------------
template < typename impl_t >
size_t bitset< impl_t >::bsr( std::true_type ) const
{
    return impl_.bsr( );
}

//--------------------------------
template < typename impl_t >
size_t bitset< impl_t >::bsr( std::false_type ) const
{
    assert( false );
    return npos( );
}

//--------------------------------
template < typename impl_t >
bool
    bitset< impl_t >::read_at( size_t const i_index, std::true_type ) const
{
    return impl_.read_at( i_index );
}

//--------------------------------
template < typename impl_t >
bool
    bitset< impl_t >::read_at( size_t const, std::false_type ) const
{
    assert( false );
    return false;
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const i_index, bool const i_value, std::true_type )
{
    impl_.write_at( i_index, i_value );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const, bool const, std::false_type )
{
    assert( false );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const i_index, std::true_type, std::true_type )
{
    impl_.write_at( i_index, std::true_type( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const, std::true_type, std::false_type )
{
    assert( false );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const i_index, std::false_type, std::true_type )
{
    impl_.write_at( i_index, std::false_type( ) );
}

//--------------------------------
template < typename impl_t >
void
    bitset< impl_t >::write_at( size_t const, std::false_type, std::false_type )
{
    assert( false );
}

//--------------------------------
template < typename impl_t >
void bitset< impl_t >::one_all( std::true_type )
{
    impl_.one_all( );
}

//--------------------------------
template < typename impl_t >
void bitset< impl_t >::one_all( std::false_type )
{
    assert( false );
}

//--------------------------------
template < typename impl_t >
void bitset< impl_t >::zero_all( std::true_type )
{
    impl_.zero_all( );
}

//--------------------------------
template < typename impl_t >
void bitset< impl_t >::zero_all( std::false_type )
{
    assert( false );
}

//--------------------------------
template < typename impl_t >
void bitset< impl_t >::flip_all( std::true_type )
{
    impl_.flip_all( );
}

//--------------------------------
template < typename impl_t >
void bitset< impl_t >::flip_all( std::false_type )
{
    assert( false );
}

//--------------------------------
template < typename impl_t >
constexpr size_t bitset< impl_t >::npos( std::true_type )
{
    return impl_t::npos( );
}

//--------------------------------
template < typename impl_t >
constexpr size_t bitset< impl_t >::npos( std::false_type )
{
    assert( false );
    return std::numeric_limits< size_t >::max( );
}

//--------------------------------
template < typename impl_t >
constexpr size_t bitset< impl_t >::size( std::true_type )
{
    return impl_t::size( );
}

//--------------------------------
template < typename impl_t >
constexpr size_t bitset< impl_t >::size( std::false_type )
{
    assert( false );
    return 0;
}
