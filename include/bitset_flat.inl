/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
/*	This file is part of Bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	Bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Bitset. If not, see <http://www.gnu.org/licenses/>.
*/
// NOTE: this file is inside detail_bitset

//----------------------------------------------------------------
template < size_t bit_count >
class flat
{
    using this_type = flat< bit_count >;

  public:
    //--------------------------------
    flat( ) noexcept
    {
        zero_all( );
    }

    //--------------------------------
    bool
        all( ) const
    {
        constexpr word_t const all_set = std::numeric_limits< word_t >::max( );
        constexpr size_t const max     = array_size - 1;

        // check all except last (or skip if this is last)
        for ( size_t i = 0; i < max; ++i )
        {
            if ( words_[i] != all_set )
            {
                return false;
            }
        }

        // check last
        constexpr size_t const end_offset = bit_count - ( max * word_size );
        constexpr size_t const end_index  = word_size - end_offset;
        word_t const           mask       = all_set >> end_index;
        return words_[max] == mask;
    }

    //--------------------------------
    bool
        any( ) const
    {
        size_t i = 0;
        do
        {
            if ( words_[i] != 0 )
            {
                return true;
            }
        } while ( ++i != array_size );
        return false;
    }

    //--------------------------------
    bool
        none( ) const
    {
        size_t i = 0;
        do
        {
            if ( words_[i] != 0 )
            {
                return false;
            }
        } while ( ++i != array_size );
        return true;
    }

    //--------------------------------
    size_t
        count( ) const
    {
        size_t sum = 0;
        size_t i   = 0;
        do
        {
            sum += detail_bitset::bitcount( words_[i] );
        } while ( ++i != array_size );
        return sum;
    }

    //--------------------------------
    size_t
        bsf( ) const
    {
        size_t i = 0;
        // for all words
        do
        {
            // if any set
            if ( words_[i] != 0 )
            {
                size_t const offset = bitScanForward( words_[i] );
                return i * word_size + offset;
            }
        } while ( ++i != array_size );

        return this_type::npos( );
    }

    //--------------------------------
    size_t
        bsr( ) const
    {
        size_t i = array_size - 1;
        // for all words (reverse iteration)
        do
        {
            // if any set
            if ( words_[i] != 0 )
            {
                size_t const offset = bitScanReverse( words_[i] );
                return i * word_size + offset;
            }
        } while ( i-- != 0 );

        return this_type::npos( );
    }

    //--------------------------------
    bool
        read_at( size_t const i_index ) const
    {
        assert( i_index < bit_count );
        size_t const     index  = i_index / word_size;
        size_t const     offset = i_index - index * word_size;
        constexpr word_t mask   = 0x01;

        return ( words_[index] & ( mask << offset ) ) > 0;
    }

#pragma warning( push )
#pragma warning( \
    disable : 4146 ) // unary minus operator applied to unsigned type, result still unsigned
    //--------------------------------
    void
        write_at( size_t const i_index, bool const i_value )
    {
        assert( i_index < bit_count );
        size_t const     index  = i_index / word_size;
        size_t const     offset = i_index - index * word_size;
        constexpr word_t mask   = 0x01;
        word_t const     value  = i_value ? 0x1 : 0x0;

        words_[index] ^= ( -value ^ words_[index] ) & ( mask << offset );
    }
#pragma warning( pop )

    //--------------------------------
    void
        write_at( size_t const i_index, std::true_type )
    {
        assert( i_index < bit_count );
        size_t const     index  = i_index / word_size;
        size_t const     offset = i_index - index * word_size;
        constexpr word_t mask   = 0x01;

        words_[index] |= mask << offset;
    }

    //--------------------------------
    void
        write_at( size_t const i_index, std::false_type )
    {
        assert( i_index < bit_count );
        size_t const     index  = i_index / word_size;
        size_t const     offset = i_index - index * word_size;
        constexpr word_t mask   = 0x01;

        words_[index] &= ~( mask << offset );
    }

    //--------------------------------
    void
        one_all( )
    {
        memset( &words_[0], 0xFF, sizeof( word_t ) * array_size );

        // unset unused bits in last word
        constexpr size_t       max        = array_size - 1;
        constexpr word_t const all_set    = std::numeric_limits< word_t >::max( );
        constexpr size_t const end_offset = bit_count - ( max * word_size );
        constexpr size_t const end_index  = word_size - end_offset;
        word_t const           mask       = all_set >> end_index;
        words_[max] &= mask;
    }

    //--------------------------------
    void
        zero_all( )
    {
        memset( &words_[0], 0, sizeof( word_t ) * array_size );
    }

    //--------------------------------
    void
        flip_all( )
    {
        size_t i = 0;
        do
        {
            words_[i] = ~words_[i];
        } while ( ++i != array_size );

        // unset unused bits in last word
        constexpr size_t const max        = array_size - 1;
        constexpr word_t const all_set    = std::numeric_limits< word_t >::max( );
        constexpr size_t const end_offset = bit_count - ( max * word_size );
        constexpr size_t const end_index  = word_size - end_offset;
        word_t const           mask       = all_set >> end_index;
        words_[max] &= mask;
    }

    //--------------------------------
    static constexpr size_t
        npos( )
    {
        return std::numeric_limits< size_t >::max( );
    }

    //--------------------------------
    static constexpr size_t
        size( )
    {
        return bit_count;
    }

  private:
    using word_t                       = uint64_t;
    static constexpr size_t word_size  = sizeof( word_t ) * 8;
    static constexpr size_t array_size = static_cast< size_t >( detail_bitset::ceil(
        static_cast< double >( bit_count ) / static_cast< double >( word_size ) ) );

    std::array< word_t, array_size > words_;
};