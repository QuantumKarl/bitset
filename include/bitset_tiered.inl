/*	This file is part of bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with bitset. If not, see <http://www.gnu.org/licenses/>.
*/
/*	This file is part of Bitset.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	Bitset is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Bitset is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Bitset. If not, see <http://www.gnu.org/licenses/>.
*/
// NOTE: this file is inside detail_bitset

//----------------------------------------------------------------
template < size_t bit_count >
class tiered
{
    using this_type = tiered< bit_count >;

  public:
    //--------------------------------
    tiered( ) noexcept
    {
        zero_all( );
    }

    //--------------------------------
    bool
        all( ) const
    {
        // check all of any_words_
        constexpr size_t const any_max = any_array_size - 1;

        // check all except last (or skip if this is last)
        for ( size_t i = 0; i < any_max; ++i )
        {
            if ( any_words_[i] != all_set( ) )
            {
                return false;
            }
        }

        {
            // check last
            constexpr size_t const end_offset = array_size - ( any_max * word_size );
            constexpr word_t const end_index  = word_size - end_offset;
            constexpr word_t const mask       = all_set( ) >> end_index;

            if ( any_words_[any_max] != mask )
            {
                return false;
            }
        }

        constexpr size_t const word_max = array_size - 1;

        // check all except last (or skip if this is last)
        for ( size_t i = 0; i < word_max; ++i )
        {
            if ( words_[i] != all_set( ) )
            {
                return false;
            }
        }

        // check last
        constexpr size_t const end_offset = bit_count - ( word_max * word_size );
        constexpr word_t const end_index  = word_size - end_offset;
        constexpr word_t const mask       = all_set( ) >> end_index;
        return words_[word_max] == mask;
    }

    //--------------------------------
    bool
        any( ) const
    {
        size_t i = 0;
        do
        {
            if ( any_words_[i] != 0 )
            {
                return true;
            }
        } while ( ++i != any_array_size );
        return false;
    }

    //--------------------------------
    bool
        none( ) const
    {
        size_t i = 0;
        do
        {
            if ( any_words_[i] != 0 )
            {
                return false;
            }
        } while ( ++i != any_array_size );
        return true;
    }

    //--------------------------------
    size_t
        count( ) const
    {
        size_t sum = 0;
        size_t i   = 0;
        do
        {
            if ( any_words_[i] != 0 )
            {
                // find where the set bits start
                size_t const any_offset = detail_bitset::bitScanForward( any_words_[i] );
                size_t const max_offset = detail_bitset::bitScanReverse( any_words_[i] );

                // calculate the index in words where the set bits start
                size_t const words_index = ( i * word_size ) + any_offset;
                size_t const max_index   = ( i * word_size ) + max_offset;

                // sum the set bits there
                size_t       j   = words_index;
                size_t const max = std::min( array_size, max_index + 1 );
                do
                {
                    sum += detail_bitset::bitcount( words_[j] );
                } while ( ++j != max );
            }
        } while ( ++i != any_array_size );

        return sum;
    }

    //--------------------------------
    size_t
        bsf( ) const
    {
        size_t i = 0;
        do
        {
            // if any set
            if ( any_words_[i] != 0 )
            {
                // scan to find set bits in any words
                size_t const any_offset = detail_bitset::bitScanForward( any_words_[i] );
                size_t const word_index = i * word_size + any_offset;
                assert( words_[word_index] != 0 );

                // scan the according word to find the set bit's index
                size_t const words_offset = detail_bitset::bitScanForward( words_[word_index] );

                // return that index
                return word_index * word_size + words_offset;
            }
        } while ( ++i != any_array_size );

        return this_type::npos( );
    }

    //--------------------------------
    size_t
        bsr( ) const
    {
        size_t i = any_array_size - 1;
        do
        {
            // if any set
            if ( any_words_[i] != 0 )
            {
                // scan to find set bits in any words
                size_t const any_offset = detail_bitset::bitScanReverse( any_words_[i] );
                size_t const word_index = i * word_size + any_offset;
                assert( words_[word_index] != 0 );

                // scan the according word to find the set bit's index
                size_t const words_offset = detail_bitset::bitScanReverse( words_[word_index] );

                // return that index
                return word_index * word_size + words_offset;
            }
        } while ( i-- != 0 );

        return this_type::npos( );
    }

    //--------------------------------
    bool
        read_at( size_t const i_index ) const
    {
        assert( i_index < bit_count );
        size_t const index  = i_index / word_size;
        word_t const offset = i_index - index * word_size;

        return ( words_[index] & ( one( ) << offset ) ) > 0;
    }

#pragma warning( push )
#pragma warning( \
    disable : 4146 ) // unary minus operator applied to unsigned type, result still unsigned
    //--------------------------------
    void
        write_at( size_t const i_index, bool const i_value )
    {
        assert( i_index < bit_count );

        // set data
        size_t const index  = i_index / word_size;
        word_t const offset = i_index - index * word_size;
        word_t const value  = i_value ? one( ) : zero( );

        words_[index] ^= ( -value ^ words_[index] ) & ( one( ) << offset );

        // set any data
        word_t const any = words_[index] != 0 ? one( ) : zero( );

        size_t const any_index  = index / word_size;
        word_t const any_offset = index - any_index * word_size;

        any_words_[any_index] ^= ( -any ^ any_words_[any_index] ) & ( one( ) << any_offset );
    }
#pragma warning( pop )

    //--------------------------------
    void
        write_at( size_t const i_index, std::true_type )
    {
        assert( i_index < bit_count );
        size_t const index  = i_index / word_size;
        word_t const offset = i_index - index * word_size;

        words_[index] |= one( ) << offset;

        // set any words
        size_t const any_index  = index / word_size;
        word_t const any_offset = index - any_index * word_size;

        any_words_[any_index] |= one( ) << any_offset;
    }

#pragma warning( push )
#pragma warning( \
    disable : 4146 ) // unary minus operator applied to unsigned type, result still unsigned
    //--------------------------------
    void
        write_at( size_t const i_index, std::false_type )
    {
        assert( i_index < bit_count );
        size_t const index  = i_index / word_size;
        word_t const offset = i_index - index * word_size;

        words_[index] &= ~( one( ) << offset );

        // set any data
        word_t const any = words_[index] != 0 ? one( ) : zero( );

        size_t const any_index  = index / word_size;
        word_t const any_offset = index - any_index * word_size;

        any_words_[any_index] ^= ( -any ^ any_words_[any_index] ) & ( one( ) << any_offset );
    }
#pragma warning( pop )

    //--------------------------------
    void
        one_all( )
    {
        // set all in words
        memset( &words_[0], 0xFF, sizeof( word_t ) * array_size );
        {
            // unset unused bits in last word
            constexpr size_t       max        = array_size - 1;
            constexpr size_t const end_offset = bit_count - ( max * word_size );
            constexpr word_t const end_index  = word_size - end_offset;
            constexpr word_t const mask       = all_set( ) >> end_index;
            words_[max] &= mask;
        }

        // set all in any words
        memset( &any_words_[0], 0xFF, sizeof( word_t ) * any_array_size );
        {
            // unset unused bits in last word
            constexpr size_t       max        = any_array_size - 1;
            constexpr size_t const end_offset = array_size - ( max * word_size );
            constexpr word_t const end_index  = word_size - end_offset;
            constexpr word_t const mask       = all_set( ) >> end_index;
            any_words_[max] &= mask;
        }
    }

    //--------------------------------
    void
        zero_all( )
    {
        memset( &words_[0], 0, sizeof( word_t ) * array_size );
        memset( &any_words_[0], 0, sizeof( word_t ) * any_array_size );
    }

//--------------------------------
#pragma warning( push )
#pragma warning( \
    disable : 4146 ) // unary minus operator applied to unsigned type, result still unsigned
    void
        flip_all( )
    {
        constexpr size_t max = array_size - 1;

        // for all except last (or skip if this is the last)
        for ( size_t i = 0; i < max; ++i )
        {
            words_[i] = ~words_[i];

            word_t const any = words_[i] != 0 ? one( ) : zero( );

            size_t const any_index =
                static_cast< size_t >( static_cast< double >( i / word_size ) + 0.5 );
            word_t const any_offset = i - any_index * word_size;

            any_words_[any_index] ^=
                ( -any ^ any_words_[any_index] ) & ( one( ) << any_offset );
        }

        words_[max] = ~words_[max];
        // unset unused bits in last word
        constexpr size_t const end_offset = bit_count - ( max * word_size );
        constexpr word_t const end_index  = word_size - end_offset;
        constexpr word_t const mask       = all_set( ) >> end_index;
        words_[max] &= mask;

        word_t const any = words_[max] != 0 ? one( ) : zero( );

        constexpr size_t const any_index =
            static_cast< size_t >( static_cast< double >( max / word_size ) + 0.5 );
        constexpr word_t const any_offset = max - any_index * word_size;

        any_words_[any_index] ^= ( -any ^ any_words_[any_index] ) & ( one( ) << any_offset );
    }
#pragma warning( pop )

    //--------------------------------
    static constexpr size_t
        npos( )
    {
        return std::numeric_limits< size_t >::max( );
    }

    //--------------------------------
    static constexpr size_t
        size( )
    {
        return bit_count;
    }

  private:
    using word_t = uint64_t;

    //--------------------------------
    static constexpr word_t
        zero( )
    {
        return static_cast< word_t >( 0 );
    }

    //--------------------------------
    static constexpr word_t
        one( )
    {
        return static_cast< word_t >( 1 );
    }

    //--------------------------------
    static constexpr word_t
        all_set( )
    {
        return std::numeric_limits< word_t >::max( );
    }

    static constexpr size_t word_size  = sizeof( word_t ) * 8;
    static constexpr size_t array_size = static_cast< size_t >( detail_bitset::ceil(
        static_cast< double >( bit_count ) / static_cast< double >( word_size ) ) );

    static constexpr size_t any_array_size = static_cast< size_t >( detail_bitset::ceil(
        static_cast< double >( bit_count ) / static_cast< double >( word_size ) /
        static_cast< double >( word_size ) ) );

    std::array< word_t, array_size >     words_;     // the data
    std::array< word_t, any_array_size > any_words_; // data to indicate if any is set in data
};